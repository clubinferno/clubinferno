<?php
    require'templates/begin.php';
?>
<div class="sponsor-container">
    <?php
            require 'php/config.php';
            $sql = "SELECT *
                    FROM foto
                    ORDER BY data DESC";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<a href=#" . $row['idFoto'] . ">
                          <div class='sponsor'>";
                    echo "<img src='img/gallery/photos/$row[nome]'/>";
                    echo "</img>
                          </div>
                          </a>";
                    echo "<div id=" . $row['idFoto'] . " class=overlay2>
	                           <div class=popup>
                                <h2>" . htmlentities(utf8_encode($row['titolo']), 0, 'UTF-8') . "</h2>
                                <a id=close href=#>&times;</a>
                                <img src='img/gallery/photos/$row[nome]'/>
                                <p>" . htmlentities(utf8_encode($row['descrizione']), 0, 'UTF-8') . "</p>
                            </div>
                        </div>";
                }
            }
        ?>
</div>
<?php
    require'templates/end.php';
?>
