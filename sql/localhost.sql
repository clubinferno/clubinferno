-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2018 at 12:26 AM
-- Server version: 5.6.33-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_clubinferno`
--

-- --------------------------------------------------------

--
-- Table structure for table `birra`
--

CREATE TABLE IF NOT EXISTS `birra` (
  `idBirra` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `descrizione` text COLLATE latin1_general_cs NOT NULL,
  `immagine` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `sito` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`idBirra`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=11 ;

--
-- Dumping data for table `birra`
--

INSERT INTO `birra` (`idBirra`, `nome`, `descrizione`, `immagine`, `sito`) VALUES
(1, 'PUNK IPA', 'THE BEER THAT BEGAN A REVOLUTION.', 'punk-ipa.png', 'https://www.brewdog.com/beer/headliners/punk-ipa'),
(2, 'KINGPIN', 'THIS IS LAGER REBORN, A TASTE SENSATION FOR THE 21ST CENTURY.', 'kingpin.png', 'https://www.brewdog.com/beer/headliners/kingpin'),
(3, '6 Luppoli Bock Rossa', '\r\nUna birra dal gusto di malto tostato con venature di caramello e liquirizia. Un’intensa luppolatura per una rossa corposa dalla spiccata personalità.\r\nGrado alcolico: 7.0% VOL', '6-luppoli-bock.jpg', 'http://www.birrificioangeloporetti.it/it/birra/6-luppoli-bock-rossa'),
(4, '9 Luppoli American IPA', 'Una birra ad alta fermentazione dall’amaro accentuato. La sua personalità esplosiva è caratterizzata da uno speciale bouquet di luppoli che conferiscono un intenso aroma floreale e agrumato.\r\nGrado Alcolico: 5.9% VOL', '9-american-ipa.png', 'http://www.birrificioangeloporetti.it/it/birra/9-luppoli-american-ipa'),
(5, '9 Luppoli Belgian Blanche', 'Un carattere vivace per una birra bianca di frumento ad alta fermentazione. A caratterizzarla è la sua speziatura unita ad un corpo morbido e avvolgente, frutto di una delicata luppolatura.\r\nGrado Alcolico: 5.2% VOL', '9-belgian.png', 'http://www.birrificioangeloporetti.it/it/birra/9-luppoli-belgian-blanche'),
(6, 'Cervisia Camallo', 'Cervisia Camallo', 'cervisia_camallo.png', NULL),
(7, '50 Nodi', 'Birra di colore ambrato, caratterizzata da lievi note di caramello e intensi profumi floreali, agrumati e di frutta esotica.\r\n\r\nIl suo carattere forte deriva da una miscela di luppoli inglesi, americani e neozelandesi che vi accompagneranno in un viaggio sensoriale ineguagliabile.\r\n\r\nABBINAMENTI\r\nGnocchetti al ragù di pecora, antunna al forno, formaggi a media stagionatura, carni alla griglia.', '50nodi_more.png', 'http://www.p3brewing.it/le-birre-p3/50-nodi.html'),
(8, '100 Nodi', 'Nell''ampio bouquet aromatico primeggia l''agrumato (pompelmo, mandarino e scorza d''arancia) affiancato da note erbacee, speziate e balsamiche.\r\n\r\nDurante la beva, note fruttate di pesca e albicocca vengono bilanciate da un''importante luppolatura.\r\n\r\nIl finale secco e il corpo bilanciato le donano una grande bevibilità.\r\n\r\nTravolgente ondata di emozioni!\r\n\r\nIl suo carattere forte deriva da una miscela di luppoli inglesi, americani e neozelandesi che vi accompagneranno in un viaggio sensoriale ineguagliabile.\r\n\r\nABBINAMENTI\r\nFormaggio pecorino arrosto, gulash, seadas.', '100nodi_more.png', 'http://www.p3brewing.it/le-birre-p3/100-nodi.html'),
(9, 'Turkunara', 'Birra dal colore scuro impenetrabile.\r\n\r\nSi caratterizza per i suoi importanti aromi fruttati, note di caffè, radice di liquirizia e di caramello, unitamente a un corpo strutturato e vellutato.\r\n\r\nAmica preziosa nelle fredde giornate invernali, dolce chiusa per un ricco banchetto.\r\n\r\nABBINAMENTI\r\nCrostini di bottarga, Carni in umido, Frutta e dolci secchi, Cioccolato fondente.', 'turkunara_more.png', 'http://www.p3brewing.it/le-birre-p3/turkunara.html'),
(10, 'Riff', 'Birra dal colore paglierino nella quale luppoli anglo-americani, con note agrumate e retrogusto secco, si fondono armoniosamente con l’elegante speziatura belga e una leggera acidità rinfrescante.\r\n\r\nOttima come aperitivo e come estintore per la sete!\r\n\r\nABBINAMENTI\r\nFormaggi a pasta filata, filetto di cavallo marinato alla griglia, sardine impanate e fritte.', 'Riff_more.png', 'http://www.p3brewing.it/le-birre-p3/riff.html');

-- --------------------------------------------------------

--
-- Table structure for table `contattosponsor`
--

CREATE TABLE IF NOT EXISTS `contattosponsor` (
  `IDdocumento` int(4) NOT NULL AUTO_INCREMENT,
  `numero` int(20) NOT NULL,
  `intestatario` varchar(50) CHARACTER SET latin1 NOT NULL,
  `fkSponsor` int(4) NOT NULL,
  PRIMARY KEY (`IDdocumento`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE IF NOT EXISTS `foto` (
  `idFoto` int(11) NOT NULL AUTO_INCREMENT,
  `titolo` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_cs DEFAULT NULL,
  `descrizione` varchar(300) CHARACTER SET latin1 COLLATE latin1_general_cs DEFAULT NULL,
  `nome` varchar(300) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idFoto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `foto`
--

INSERT INTO `foto` (`idFoto`, `titolo`, `descrizione`, `nome`, `data`) VALUES
(1, 'Bancone', NULL, '1.jpg', '2007-08-09'),
(2, NULL, NULL, '2.jpg', '2017-08-01'),
(3, NULL, NULL, '3.jpg', '0000-00-00'),
(4, NULL, NULL, '4.jpg', '0000-00-00'),
(5, NULL, NULL, '5.jpg', '0000-00-00'),
(6, NULL, NULL, '6.jpg', '0000-00-00');

--
-- Triggers `foto`
--
DROP TRIGGER IF EXISTS `Auto Data`;
DELIMITER //
CREATE TRIGGER `Auto Data` BEFORE INSERT ON `foto`
 FOR EACH ROW IF(isnull(NEW.data))
THEN
SET NEW.data=CURDATE();
END IF
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `notizia`
--

CREATE TABLE IF NOT EXISTS `notizia` (
  `idNotizia` int(11) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `titolo` varchar(50) CHARACTER SET latin1 NOT NULL,
  `testo` text CHARACTER SET latin1 NOT NULL,
  `immagine` char(20) COLLATE latin1_general_cs NOT NULL DEFAULT 'news.png',
  PRIMARY KEY (`idNotizia`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs ROW_FORMAT=COMPACT AUTO_INCREMENT=8 ;

--
-- Dumping data for table `notizia`
--

INSERT INTO `notizia` (`idNotizia`, `data`, `titolo`, `testo`, `immagine`) VALUES
(1, '2017-07-03', 'Birra doppio malto in sconto', 'Controllate il catalogo delle birre', 'news.png'),
(5, '2017-07-04', 'Sconto sul kebab d''asporto', 'Sconto sul kebab d''asporto ordinato dalla pizzeria la trottola.', 'kebab.png'),
(6, '2017-07-05', 'Pizza in sconto', 'Anche la pizza è in sconto dalla pizzeria la trottola', 'pizza.png'),
(7, '2017-07-01', 'Nuove birre in catalogo', 'Nuove birre tedesche rosse in catalogo', 'news.png');

--
-- Triggers `notizia`
--
DROP TRIGGER IF EXISTS `Default Date`;
DELIMITER //
CREATE TRIGGER `Default Date` BEFORE INSERT ON `notizia`
 FOR EACH ROW IF(isnull(NEW.data))
THEN
SET NEW.data=CURDATE();
END IF
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pagamentosponsor`
--

CREATE TABLE IF NOT EXISTS `pagamentosponsor` (
  `idPagamento` int(11) NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `anno` year(4) NOT NULL,
  `fkSponsor` int(4) NOT NULL,
  PRIMARY KEY (`idPagamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sponsor`
--

CREATE TABLE IF NOT EXISTS `sponsor` (
  `IDsponsor` int(4) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `descrizione` text COLLATE latin1_general_cs NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `sito` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `immagineLogo` varchar(50) COLLATE latin1_general_cs NOT NULL,
  PRIMARY KEY (`IDsponsor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sponsor`
--

INSERT INTO `sponsor` (`IDsponsor`, `nome`, `descrizione`, `email`, `sito`, `immagineLogo`) VALUES
(1, 'Trachite Artigiana', 'L''azienda, nata nei primi mesi del 2004, è giovane e flessibile, punta a consolidare la sua posizione sul mercato come una delle principali segherie di trachite. \r\n\r\nPunto di forza dell''azienda è la coltivazione di più cave per l''estrazione e la lavorazione di 6 tipi differenti di trachite, varietà che permette di poter disporre di grosse quantità di trachite e una varia gamma cromatica. Lavorazioni speciali e costi contenuti rendono la trachite una valida alternativa a pietre come il granito. \r\n\r\nL''azienda si posiziona oggi sul mercato come interlocutrice attenta e desiderosa di offrire, un prodotto di qualità a prezzi sicuramente concorrenziali, uniti ad una seria politica di vendita e preparazione professionale del team aziendale.', 'trachiteartigiana@tiscali.it', 'http://www.trachiteartigiana.com', 'logo-trachite.svg'),
(2, 'Dancing With Sloths Music', 'Dancing With Sloths Music è uno studio di registrazione completamente D.I.Y. gestito da Fabio Sussarellu.', 'dancingwithslothsmusic@gmail.com', NULL, 'logo-sloths.svg'),
(3, 'F.D. DISTRIBUZIONE BEVANDE S.R.L.', 'Via G. Dore, 8\r\n07044 Ittiri SS\r\nTel 079/441624\r\nFax 079/441624', NULL, 'http://www.paginebianche.it/ittiri/fd-distribuzione-bevande.7360506', 'logo-franco-deiana.svg');

-- --------------------------------------------------------

--
-- Table structure for table `utente`
--

CREATE TABLE IF NOT EXISTS `utente` (
  `idUtente` int(4) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `cognome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `username` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `dataNascita` date NOT NULL,
  `codiceTessera` int(5) DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUtente`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=27 ;

--
-- Dumping data for table `utente`
--

INSERT INTO `utente` (`idUtente`, `nome`, `cognome`, `username`, `dataNascita`, `codiceTessera`, `email`, `password`, `admin`) VALUES
(22, 'Fabio', 'Sussarellu', 'Fabi0_Z', '1998-10-15', NULL, 'sussarellu.fabio@gmail.com', '$2y$10$SKRTU2R3VgC7nm2faM6o..hkg2tJHNUx5cWXFtpX9H4DOx/Dn.1qW', 1),
(12, 'Giovanni', 'Orani', 'Moriarty', '1996-08-08', NULL, 'heavycliff@gmail.com', '$2y$10$MVaxdTGol63jwhBgL0Po3eIBVAoZJqbAURNOt9vS/NVexwKPKxBb2', 1),
(13, 'Stefano', 'Leoni', 'Mr. President 80', '1980-12-23', NULL, 'samcro.1980@gmail.com', '$2y$10$jP7txZiP/ANV5J34RM7sres3GnJxL7fQ1bgkesw2rGq7d0YIM34Ki', 1),
(15, 'Nicole', 'Mura', 'Hysteria98', '1998-08-20', NULL, 'nicole.mura2@gmail.com', '$2y$10$zuMS81pwbElmdoluNuxO2ObOHaHHBQpemVqj/0DR3JtwD0vODBKNS', 0),
(16, 'Nicole', 'Mura', 'Hysteria.98', '1998-08-20', NULL, 'nicole.mura@libero.it', '$2y$10$Q9YtOXsLR3MTrjqcQApZCOxuwLI1JQRVo6zI2O.TpmgZYFTW4JCha', 0),
(23, 'A', 'A', 'A', '2017-08-01', NULL, 'A', '$2y$10$HBK3YGWyitga7pgD.eDnruk/YaWJrsXRZi94ipTKxjJVcNB6isY4m', 0),
(24, 'Pino', 'Pinguino', 'Peppa', '2018-08-11', NULL, 'Peppa', '$2y$10$9qofQGEo.xxRoJh1Cqc.3ePWaq5uughWetI5uK.sCKIVetRCLssl2', 0),
(25, 'B', 'B', 'B', '2017-12-30', NULL, 'B', '$2y$10$/CH15nc.CPdTrZomNJoJQ.GMFaEHY2iKSumo7C9F0zW2GG2ImwkdK', 0),
(26, 'Diego', 'Demelas', 'margian3t@gmail.com', '1986-06-21', NULL, 'margian3t@gmail.com', '$2y$10$rP6N4lyr8qPphkaq4fXKC.Pm2HKY1iJUMx1jMIsztmZF4BXdI8uyW', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
