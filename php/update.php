<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        require 'config.php';
        $mail = stripslashes($_POST["newMail"]);
        $user = stripslashes($_POST["newUser"]);
        $password = stripslashes($_POST["newPassword"]);
        $data = $_POST["newData"];
        $checkPassword = $_POST["password"];

        $mail = mysqli_real_escape_string($conn, $mail);
        $user = mysqli_real_escape_string($conn, $user);
        $password = mysqli_real_escape_string($conn, $password);
        $checkPassword = mysqli_real_escape_string($conn, $checkPassword);
        $data = mysqli_real_escape_string($conn, $data);

        // Estrapolo l'ID dell'Utente in base al COOKIE contenente l'username
        $sql = "SELECT * FROM utente WHERE username='$_COOKIE[username]'";
        $resultset = mysqli_query($conn, $sql);
        $row = mysqli_fetch_assoc($resultset);
        $dbPassword = $row['password'];
        $userID = $row['idUtente'];

        // Controllo che la password sia corretta
        $passOK = password_verify($checkPassword, $dbPassword);

        // Controllo i valori dei booleani nella condizione dell'if e controllo che la password sia corretta
        if ($passOK) {
            // Controllo che il nuovo username non sia già presente sul database o che se lo sia appartenga all'utente stesso
            $sql = "SELECT * FROM utente WHERE username='$user'";
            $resultset = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($resultset);
            if (!isset($row["idUtente"])) {
                $userOK = true;
            } elseif ($row["idUtente"] == $userID) {
                $userOK = true;
            } else {
                $userOK = false;
            }

            // Controllo che la nuova email non sia già presente sul database o che se lo sia appartenga all'utente stesso
            $sql = "SELECT * FROM utente WHERE email='$mail'";
            $resultset = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($resultset);
            if (!isset($row["idUtente"])) {
                $mailOK = true;
            } elseif ($row["idUtente"] == $userID) {
                $mailOK = true;
            } else {
                $mailOK = false;
            }
            if ($mailOK and $userOK) {
                // Controllo che il campo della nuova password non sia vuoto, se lo è inserisco la vecchia dal DB
                if ($password == "") {
                    $passwordCriptata = $dbPassword;
                } else {
                    $passwordCriptata = password_hash($password, PASSWORD_DEFAULT);
                }
                $sql = "UPDATE utente
                        SET username = '$user',
                        email = '$mail',
                        password = '$passwordCriptata',
                        dataNascita = '$data'
                        WHERE idUtente = '$userID'";
                if ($conn->query($sql) === true) {
                    $end = strtotime('2037-12-31');
                    setcookie("username", $user, $end, "/");
                    setcookie("nome", $user, $end, "/");
                    setcookie("password", $passwordCriptata, $end, "/");
                    header("location: ../user");
                    exit();
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            } else {
                session_start();
                $_SESSION["valid_register_email"] = $mailOK;
                $_SESSION["valid_register_username"] = $userOK;
                header("location: ../user");
            }
        } else {
            session_start();
            $_SESSION["valid_password"] = $passOK;
            header("location: ../user");
            exit();
        }
        $conn->close();
    } else {
        header("location: ../home");
    }
