<?php
    require'templates/begin.php';
?>
<div class="admin">
    <form method="post" action="php/promote.php">
        <h2>Aggiungi/Rimuovi amministratore</h2>
        <select name="adminPromote">
                    <?php
                        require("php/config.php");
                        $sql = "SELECT idUtente, username, admin
                                FROM utente
                                ORDER BY username";
                        $out = $conn->query($sql);
                        if ($out->num_rows > 0) {
                            while ($row = $out->fetch_assoc()) {
                                $adminText = "";
                                if ($row["admin"] == 1) {
                                    $adminText = " (admin)";
                                }
                                echo "<option value=" . $row["idUtente"] . ">" . htmlentities(utf8_encode($row["username"]), 0, 'UTF-8') . $adminText . "</option>";
                            }
                        }
                    ?>
                </select>
        <input type="submit" value="Cambia status">
    </form>
</div>
<div class="admin">
    <form method="post" action="php/news.php">
        <h2>Aggiungi/Modifica news</h2>
        <h3>Titolo</h3>
        <input type="text" name="TitoloNews" required>
        <h3>Testo</h3>
        <input type="text" name="TestoNews" required>
        <br>
        <input type="submit" value="Posta">
        <br>
        <select name="adminPromote">
                    <?php
                        require("php/config.php");
                        $sql = "SELECT idNotizia, titolo
                                FROM notizia
                                ORDER BY data";
                        $out = $conn->query($sql);
                        if ($out->num_rows > 0) {
                            while ($row = $out->fetch_assoc()) {
                                echo "<option value=" . $row["idNotizia"] . ">" . htmlentities(utf8_encode($row["titolo"]), 0, 'UTF-8') . "</option>";
                            }
                        }
                    ?>
                </select>
        <input type="submit" value="Modifica">
    </form>
</div>
<?php
    require'templates/end.php';
?>
