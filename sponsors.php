<?php
    require'templates/begin.php';
?>
<div class="sponsor-container">
    <?php
            require 'php/config.php';
            $sql = "SELECT * FROM sponsor";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<a href=#" . $row['IDsponsor'] . ">
                          <div class='sponsor'>";
                    echo "<img src='img/sponsor/$row[immagineLogo]'/>";
                    echo "<h2>" . htmlentities(utf8_encode($row['nome']), 0, 'UTF-8') . "</h2>";
                    echo "</img>
                          </div>
                          </a>";
                    echo "<div id=" . $row['IDsponsor'] . " class=overlay2>
	                           <div class=popup>
                                <h2>" . htmlentities(utf8_encode($row['nome']), 0, 'UTF-8') . "</h2>
                                <a id=close href=#>&times;</a>
                                <img src='img/sponsor/$row[immagineLogo]'/>
                                <p>" . htmlentities(utf8_encode($row['descrizione']), 0, 'UTF-8') . "</p>
                                <br>";
                    if (!is_null($row["sito"])) {
                        echo        "<a href='$row[sito]'><i class='fa fa-globe' aria-hidden='true'></i> Vai al sito!</a>";
                    }
                    if (!is_null($row["email"])) {
                        echo "<a href='mailto:" . $row['email'] . "'><i class='fa fa-envelope' aria-hidden='true'></i> Scrivi un'email</a>";
                    }
                    echo "</div>
                        </div>";
                }
            }
        ?>
</div>
<?php
    require 'templates/end.php';
?>
