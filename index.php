<?php
    session_start();
    if (isset($_COOKIE['username']) and isset($_COOKIE['password'])) {
        header("location: home");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="it">

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title>Club Inferno</title>
    <meta name="description" content="Club Inferno, Centro Sportivo Educativo Nazionale">
    <meta name="keywords" content="bar, rock, inferno, circolo, club inferno, club, radical spaghetti">
    <meta name="author" content="Radical Spaghetti">

    <!-- Favicons -->

    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- Stylesheet and other stuffs -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="fonts/RM_Typerighter_Regular/RM_Typerighter_Regular.css" />
    <link rel="stylesheet" type="text/css" href="fonts/RM_Typerighter_old_Regular/RM_Typerighter_old_Regular.css" />
    <link rel="stylesheet" type="text/css" href="fonts/RickGriffin/RickGriffin.css" />
    <link rel="stylesheet" type="text/css" href="fonts/Font_Awesome/css/font-awesome.css" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/hidePopup.js"></script>
    <script type="text/javascript" src="js/showPassword.js"></script>
    <script type="text/javascript" src="js/noSpaces.js"></script>
</head>

<body id="background">
    <div class="overlay">
        <div class="welcome">
            <img src="img/logo-shadow.svg" id="desktop" />
            <img src="img/logo-mobile.svg" id="mobile" />
            <br/>
            <h2>Centro Sportivo Educativo Nazionale</h2>
            <h3>Iscriviti per accedere alle sezioni esclusive!</h3>
            <br/>
            <ul>
                <li>
                    <a href="#sign-up"><button id="buttonSocio">Diventa Socio</button></a>
                </li>
                <li>
                    <a href="#login"><button id="buttonAccedi">Accedi</button></a>
                </li>
            </ul>
        </div>
    </div>

    <!-- LOGIN -->
    <div id="login" class="modal">
        <form class="modal-content" action="php/login.php" method="post">
            <a href="#"><span class="close">&times;</span></a>
            <div>
                <label>
                    <?php
                        if (isset($_SESSION["invalid_user"])) {
                            echo "<h4 style='color:red'>Nome utente errato!</h4>";
                            unset($_SESSION["invalid_user"]);
                        } else {
                            echo "<h4>Nome utente</h4>";
                        }
                    ?>
                </label>
                <input id="noSpace" type="text" placeholder="Inserisci il nome utente" name="username" required>
                <label>
                    <?php
                            if (isset($_SESSION["invalid_password"])) {
                                echo "<h4 style='color:red'>Password errata!</h4>";
                                unset($_SESSION["invalid_password"]);
                            } else {
                                echo "<h4>Password</h4>";
                            }
                        ?>
                </label>
                <input type="password" placeholder="Inserisci la password" name="password" id="passwordLogin" required/><button type="button" id="showPassword" class="fa fa-eye" aria-hidden="true"></button>
                <h4 id="labelRestaConnesso"><input name="resta-connesso" type="checkbox" checked="checked"> Resta Connesso</h4>
                <button name="login" value="Accedi" type="submit" id="buttonLogin">Accedi</button>
                <span class="psw"><a id="label3" href="#">Hai dimenticato la password?</a></span>
            </div>
        </form>
    </div>
    <!-- SIGN UP -->
    <div id="sign-up" class="modal">
        <form class="modal-content" action="php/subscribe.php" method="post">
            <a href="#"><span class="close">&times;</span></a>
            <div>
                <label><h4 id="labelRegistrazione">Nome</h4></label>
                <input type="text" placeholder="Inserisci il tuo nome" name="name" required>
                <label><h4 id="labelRegistrazione">Cognome</h4></label>
                <input type="text" placeholder="Inserisci il tuo cognome" name="surname" required>
                <label>
                    <?php
                            if (isset($_SESSION["invalid_register_email"])) {
                                echo "<h4 id='labelRegistrazione' style='color:red'>E-Mail già in uso!</h4>";
                                unset($_SESSION["invalid_register_email"]);
                            } else {
                                echo "<h4 id='labelRegistrazione'>E-mail</h4>";
                            }
                        ?>
                </label>
                <input type="text" placeholder="Inserisci la tua email" name="email" required>
                <label>
                    <?php
                            if (isset($_SESSION["invalid_register_username"])) {
                                echo "<h4 id='labelRegistrazione' style='color:red'>Nome utente già in uso!</h4>";
                                unset($_SESSION["invalid_register_username"]);
                            } else {
                                echo "<h4 id='labelRegistrazione'>Nome Utente</h4>";
                            }
                        ?>
                </label>
                <input id="registerUsername" type="text" placeholder="Inserisci il nome utente" name="username" required>
                <label><h4 id="labelRegistrazione">Password</h4></label>
                <input type="password" placeholder="Inserisci una password" name="password" id="passwordRegister" required><button type="button" id="showPassword2" class="fa fa-eye" aria-hidden="true"></button>
                <label><h4 id="labelRegistrazione">Data di Nascita</h4></label>
                <input type="date" id="datepicker" placeholder="Inserisci la data di nascita" name="dataNascita" required>
                <button type="submit" id="buttonRegistrazione">Registrati</button>
            </div>
        </form>
    </div>

</body>
<script>
    hidePopup();
    showPassword();
    noSpaces();
</script>

</html>
