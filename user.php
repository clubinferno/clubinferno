<?php
    require'templates/begin.php';
    $defaultEmail = $row["email"];
    $defaultDataNascita = $row["dataNascita"];
?>
<form action="php/update.php" method="post">
    <div class="user-panel">
        <label>
                    <?php
                            if (isset($_SESSION["valid_register_username"])) {
                                if ($_SESSION["valid_register_username"]) {
                                    echo "<h4 id='labelRegistrazione'>Nome Utente</h4>";
                                } else {
                                    echo "<h4 style='color:red'>Nome utente già in uso!</h4>";
                                    unset($_SESSION["valid_register_username"]);
                                }
                            } else {
                                echo "<h4 id='labelRegistrazione'>Nome Utente</h4>";
                            }
                        ?>
                </label>
        <input type="text" placeholder="Inserisci un username" name="newUser" required value=<?php echo "'". $_COOKIE[ 'username'] . "'"; ?>>
        <label>
                    <?php
                            if (isset($_SESSION["valid_register_email"])) {
                                if ($_SESSION["valid_register_email"]) {
                                    echo "<h4 id='labelRegistrazione'>E-mail</h4>";
                                } else {
                                    echo "<h4 style='color:red'>E-Mail già in uso!</h4>";
                                    unset($_SESSION["valid_register_email"]);
                                }
                            } else {
                                echo "<h4 id='labelRegistrazione'>E-mail</h4>";
                            }
                        ?>
                </label>
        <input type="text" placeholder="Inserisci un indirizzo email" name="newMail" required value=<?php echo "'". $defaultEmail . "'"; ?>>
        <label>
                    <h4>Data di Nascita</h4>
                </label>
        <input type="date" placeholder="Inserisci la data di nascita" name="newData" required value=<?php echo "'". $defaultDataNascita . "'"; ?>>
        <label>
                    <h4>Modifica password</h4>
                </label>
        <input type="password" placeholder="Inserisci la nuova password" name="newPassword" id="passwordLogin"><button type="button" id="showPassword" class="fa fa-eye" aria-hidden="true"></button>
        <label>
                    <?php
                            if (isset($_SESSION["valid_password"])) {
                                if ($_SESSION["valid_password"]) {
                                    echo "<h4 id='labelLogin'>Inserisci la Password</h4>";
                                } else {
                                    echo "<h4 id='labelLogin' style='color:red'>Password errata!</h4>";
                                    unset($_SESSION["valid_password"]);
                                }
                            } else {
                                echo "<h4 id='labelLogin'>Inserisci la Password</h4>";
                            }
                        ?>
                </label>
        <input type="password" placeholder="Inserisci la tua attuale password" name="password" required>
        <button type="submit" id="buttonRegistrazione">Modifica</button>
        <button action="php/delete.php" id="buttonRegistrazione">Elimina il tuo account</button>
    </div>
</form>
<?php
    require'templates/end.php';
?>
