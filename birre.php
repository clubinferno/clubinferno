<?php
    require'templates/begin.php';
?>
<div class="sponsor-container">
    <?php
            require 'php/config.php';
            $sql = "SELECT *
                    FROM birra
                    ORDER BY nome";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<a href=#" . $row['idBirra'] . ">
                          <div class='sponsor'>";
                    echo "<img src='img/birre/$row[immagine]'/>";
                    echo "<h2>" . htmlentities(utf8_encode($row['nome']), 0, 'UTF-8') . "</h2>";
                    echo "</img>
                          </div>
                          </a>";
                    echo "<div id=" . $row['idBirra'] . " class=overlay2>
	                           <div class=popup>
                                <h2>" . htmlentities(utf8_encode($row['nome']), 0, 'UTF-8') . "</h2>
                                <a id=close href=#>&times;</a>
                                <img src='img/birre/$row[immagine]'/>
                                <p>" . htmlentities(utf8_encode($row['descrizione']), 0, 'UTF-8') . "</p>";
                    if (!is_null($row["sito"])) {
                        echo "<a href='$row[sito]'><i class='fa fa-globe' aria-hidden='true'></i> Vai al sito!</a>";
                    }
                    echo "</div>
                        </div>";
                }
            }
        ?>
</div>
<?php
    require'templates/end.php';
?>
